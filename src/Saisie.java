
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Saisie {

	
	public static void main(String[] args)    {
		
	
		
		MoteurRPN m = new MoteurRPN();
		
		JOptionPane jop3 = new JOptionPane();
		int i=0;
				while (true)
				{
					i++;
					
					System.out.println("Veuillez saisir un chiffre ou une operation :");
					
					Scanner sc = new Scanner(System.in);
				String str = sc.nextLine(); 
				
				if (str.length()==1 && "*-+/1234567890".contains(str))
				{
					char c = str.charAt(0);
					
					if (c=='+')
					{
						
                      
						try {
							m.appliquerOperation(Operation.plus);
							m.afficherOperandes();
						} catch (Exception e) {
							jop3.showMessageDialog(null,e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);

							
						}
						
						
					
					}
					else 	if (c=='-')
					{
                      
						try {
							m.appliquerOperation(Operation.moins);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							jop3.showMessageDialog(null,e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);

						}
						m.afficherOperandes();
					
					}
					else if (c=='*')
					{
                      
						try {
							m.appliquerOperation(Operation.mult);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							jop3.showMessageDialog(null,e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);

						}
						m.afficherOperandes();
					
					}
					else	if (c=='/')
					{
                      
						try {
							m.appliquerOperation(Operation.div);
						} catch (Exception e) {
							jop3.showMessageDialog(null,e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);

							
						}
						m.afficherOperandes();
					
					}
					else
					{
						double op = Double.parseDouble(str);
						m.enregistrerOperande(op);
						m.afficherOperandes();
					}
				}
					
				else if (str.equals("exit"))
					break;
				else 
					{if (i>=1)
						jop3.showMessageDialog(null,"saisie non accept�", "Erreur", JOptionPane.ERROR_MESSAGE);
					}
					}
				
				System.out.println("finish");
		
	}
	
	
	
}
