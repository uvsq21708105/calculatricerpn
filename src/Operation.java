
public enum Operation  {

	plus('+') {
		public double eval (double a, double b)
		{
			return a+b;
		}
	},
	
	moins('-') {
		public double eval (double a, double b)
		{
			return a-b;
		}
	},
	
	div('/') {
		public double eval (double a, double b) throws Exception
		{
			if (b==0)
			{
				MoteurRPN.stack.add(b);
				MoteurRPN.stack.add(a);
				throw new Exception("division impossible sur 0");
			}
				
			
			return a/b;
		}
	},
	
	mult('*') {
		public double eval (double a, double b)
		{
			return a*b;
		}
	};
	
	private char symbole;
	
	Operation (char sym) 
	{
		
			this.symbole=sym ;
		
	}
	
	public abstract double eval (double op1,double op2) throws Exception  ;
	
	
	
	
}
