



import org.junit.Before;


import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

/**
 *
 * @author yassine
 */
public class TestPile {
    
	 
          
	 @Test
	public void testMultiply() throws Exception {
          
         
	int a, b;
        double res;
	a = 5; 
        b  = 5; 
        res = a * b;
        assertThat(res, is(equalTo(Operation.mult.eval(a,b))));
	}


	@Test
	public void testDivide() throws Exception  {
            
            
	int a, b;
        double res;
	a = 5; 
        b  = 5; 
        res = a / b;
        assertThat(res, is(equalTo(Operation.div.eval(a,b))));
}
	
        @Test
        public void testAdd() throws Exception  {
            
	int a, b;
        double res;
	a = 5; 
        b  = 5; 
        res = a + b;
        assertThat(res, is(equalTo(Operation.plus.eval(a,b))));
}
	@Test
	public void testSubstract() throws Exception  {
          
	double a, b, res;
	a = 5; 
        b  = 5; 
        res = a - b;
        assertThat(res, is(equalTo( Operation.moins.eval(a,b))));
	
}
}
