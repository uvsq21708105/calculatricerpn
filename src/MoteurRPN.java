
	import java.util.ArrayDeque;
	import java.util.Deque;
	import java.util.EmptyStackException;
	import java.util.Stack ;
	import java.util.Vector;
	public class MoteurRPN extends Vector {

		 static Deque<Double> stack = new ArrayDeque<Double>();
		
		 
		
		public MoteurRPN() {
			
		}

		public  boolean enregistrerOperande(double operande)
		{
			stack.add(operande);
			
			return true;
		}
		
		public double depiler() throws EmptyStackException
		{
			double v  ;
			if (!stack.isEmpty())
			v=(Double) stack.removeLast();
			
			else throw new EmptyStackException();
			
			return v;
		}
		
		public boolean appliquerOperation(Operation o) throws Exception 
		{
			if( stack.size()<2 )
				throw new Exception("nombre d'op�rande inssufisant ");
			
			double op1 = this.depiler();
			double op2 = this.depiler();
			
			 this.enregistrerOperande(o.eval(op1, op2));
			
				
			return true;
			
			
			
		}
		
		public void afficherOperandes()
		{
			System.out.println(stack.toString());
		}
		
		

		
		
	}

